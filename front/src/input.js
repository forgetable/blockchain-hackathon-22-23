import './css/input.css'
import './css/normalize.css'
import './css/shared.css'
import 'babel-polyfill'

window.onload = () => {
  [...document.querySelectorAll('.form_trackable')].forEach(input => input.onchange = e => {
    let allDone = true
    const allfields = [...document.querySelectorAll('.form_trackable')].reduce((obj, next) => {
      const { field } = next.dataset
      obj[field] = next.value
      return obj
    }, {})
    for(let field in allfields) {
      if(!allfields[field]) allDone = false
    }
    if(allDone) {
      document.getElementById('allDone').style.color = 'red'
      setTimeout(async () => {
        const elem = document.getElementById('rfidReady')
        elem.style.color = '#fff'
        elem.style.background = '#083D77'
        document.getElementById('allDone').style.color = 'green'
        const body = [...document.querySelectorAll('.form_trackable')].reduce((obj, next) => {
          const { field } = next.dataset
          obj[field] = field.includes('date') ?
            new Date(Date.now()) : field.includes('photo') ? '' : next.value
          return obj
        }, {})
        const req = await fetch('http://localhost:3000/api/v0/add', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify(body)
        })
      }, 3000)
    }
    else {
      document.getElementById('allDone').style.color = 'gray'
    }
  })
}
