import './css/mvd.css'
import './css/normalize.css'
import './css/shared.css'
import 'babel-polyfill'

const getById = id => document.getElementById(id)

const showTab = id => {
  [...document.querySelectorAll('.mvd_search_item')].forEach(tab => tab.style.display = 'none')
  getById(id).style.display = 'block'
}

const showMvdPage = id => {
  [...document.querySelectorAll('.mvd_page')].forEach(page => page.style.display = 'none')
  getById(id).style.display = 'none'
  getById(id).style.display = 'block'
}

const openProfile = person => {
  const { description, rfid } = person
  showMvdPage('mvdProfile')
  getById('profileDesc').innerHTML = description
  getById('profileRFID').innerHTML = rfid
  getById('profileDesc0').innerHTML = description
  getById('profileRFID0').innerHTML = description
}

const generateProfileItem = person => {
  const { name, surname, patronymic } = person
  return `
    <div style="display: flex; padding: 20px; justify-content: space-between">
      <div style="padding: 10px">${surname} ${name} ${patronymic}</div>
      <div data-info="${JSON.stringify(person)}" style="padding: 10px; cursor: pointer" class="mvd_search_item-button">
        Открыть информацию
      </div>
    </div>
  `
}

const setupListListeners = () => [...document.querySelectorAll('.mvd_search_item')].forEach(button => {
  button.onclick = e => {
    const person = JSON.parse(e.target.dataset.info)
    openProfile(person)
  }
})

const fetchAPI = async ({ path, data }) => {
  const req = await fetch(path, {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  const json = await req.json()
  return json
}
const resolveFormData = form => [...form.querySelectorAll('.mvd_search_input')].reduce((obj, next) => {
  const { field } = next.dataset
  obj[field] = field.includes('date') ?
    new Date(Date.now()) : field.includes('photo') ? '' : next.value
  return obj
}, {})

window.onload = () => {
  showMvdPage('mvdProfile')
  const resultsPage = getById('mvdResults');
  [...document.querySelectorAll('.mvd_search_tab')].forEach(item => {
    item.onclick = () => {
      const { tab } = item.dataset
      showTab(tab)
    }
  })
  getById('searchFIOForm').onsubmit = async e => {
    e.preventDefault()
    const data = resolveFormData(e.target)
    const res = await fetchAPI({data, path: 'http://localhost:3000/api/v0/findbyname'})
    resultsPage.innerHTML = res.map(person => generateProfileItem(person)).join('')
    showMvdPage('mvdResults')
    setupListListeners()
  }
  getById('searchDocsForm').onsubmit = async e => {
    e.preventDefault()
    const data = resolveFormData(e.target)
    const res = await fetchAPI({data, path: 'http://localhost:3000/api/v0/findbydoc'})
    resultsPage.innerHTML = res.map(person => generateProfileItem(person)).join('')
    showMvdPage('mvdResults')
    setupListListeners()
  }
}
