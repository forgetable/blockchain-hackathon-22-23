const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('mini-css-extract-plugin')
const autoprefixer = require('autoprefixer')

const options = {
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      {
        test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        loader: 'file-loader',
        options: {
          name: 'public/media/[name].[ext]',
          publicPath: url => path.join('/Images', url)
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'postcss-loader',
            options: { plugins: [autoprefixer()] }
          }
        ]
      },
      {
        test: /js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: { presets: ['env', 'stage-2', 'stage-0'] }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '../../StyleSheets[name].css'
    })
  ]
}


const out = [
	{
    ...options,
    entry: './src/input.js',
    output: {
      path: __dirname,
      filename: './build/input.js'
    }
	},
  {
    ...options,
    entry: './src/mvd.js',
    output: {
      path: __dirname,
      filename: './build/mvd.js'
    }
  }
]


module.exports = out
