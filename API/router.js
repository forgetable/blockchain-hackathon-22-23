const express = require('express');
const router = express.Router();
const controllers = require('./controller');

//API
router.post('/add', controllers.addPerson);
router.post('/findbyname', controllers.findPersonByFIO);
router.post('/findbydoc', controllers.findPersonByDoc);
router.get('/',function(req, res) {
    console.log('OK');
});



module.exports = router;