const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const personScheme = new Schema({
    name: String,
    surname: String,
    patronymic: String,
    doc_ser: Number,
    doc_num: Number,
    birthday: Date,
    doc_date: Date,
    photo: Buffer,
    hash: String,
    description: String
});

mongoose.connect('mongodb://localhost/test');

const Person =  mongoose.model("Person", personScheme);

module.exports = Person;