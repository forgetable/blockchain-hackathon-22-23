const mongoose = require('mongoose');
const Person = require('./model');
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/test');



exports.addPerson = (req,res) => {
    const { body = {} } = req;
    const { name, surname, patronymic, doc_ser, doc_num, birthday, doc_date, description, photo, hash = 'qwjrh32hj3j' } = body;
    Person.find({doc_num: doc_num, doc_ser: doc_ser},(err,data) => {
        if(data.length === 0){
            let newPerson = new Person({
                name,
                surname,
                patronymic,
                doc_ser,
                doc_num,
                birthday,
                doc_date,
                photo,
                hash,
                description
            });
            newPerson.save();
            res.send('ok');
        }else{
            res.send('already exist');
        }
    });
}

exports.findPersonByFIO = (req, res) => {
    const {body = {} } = req;
    const { name, surname, patronymic } = body;
    Person.find({name: name, surname: surname, patronymic: patronymic}, (err,data)=>{
        res.send(data);
    });
}

exports.findPersonByDoc = (req, res) => {
    const {body = {} } = req;
    const {doc_num, doc_ser} = body;
    Person.find({doc_num: doc_num, doc_ser: doc_ser}, (err,data)=>{
        res.send(data);
});
}

